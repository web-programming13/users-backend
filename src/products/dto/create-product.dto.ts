import {
  IsNotEmpty,
  IsPositive,
  MinLength,
  IsNumber,
  IsString,
} from 'class-validator';

export class CreateProductDto {
  @MinLength(8)
  @IsNotEmpty()
  @IsString()
  name: string;

  @IsNotEmpty()
  @IsPositive()
  @IsNumber()
  price: number;
}
